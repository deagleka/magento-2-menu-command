#!/usr/bin/env bash

project_folder=$1
is_opcache=$2
is_redis=$3
redis_server_host=$4
domain=$5

if [ ${is_opcache} -eq 1 ];then
    #clear opcache
    echo "Clear Opcache..."
    RANDOM_NAME=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 13)
    RANDOM_NAME=opcache_${RANDOM_NAME}

    #apply for both root_folder and root_folder pointed to pub
    echo "<?php opcache_reset();" > ${project_folder}/${RANDOM_NAME}.php
    echo "<?php opcache_reset();" > ${project_folder}/pub/${RANDOM_NAME}.php

    curl -o -I -L -s -w "Response code %{http_code}\n" ${domain}${RANDOM_NAME}.php

    echo ${domain}${RANDOM_NAME}.php

    rm -f ${project_folder}/${RANDOM_NAME}.php
    rm -f ${project_folder}/pub/${RANDOM_NAME}.php
fi

if [ ${is_redis} -eq 1 ];then
    #clear redis
    echo "Clear Redis Cache"
    redis-cli -h ${redis_server_host} flushdb
fi
