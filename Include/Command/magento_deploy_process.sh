#!/usr/bin/env bash

project_folder=$1
php=$2
static_content_params=$3
mode=$4
is_redis=$5
is_opcache=$6
redis_server_host=$7
domain=$8

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${DIR}/../../index.sh "${php}" "" "${project_folder}" "${domain}"

cd ${project_folder}
modman repair --force

external_cache_reset "${is_opcache}" "${is_redis}" "${redis_server_host}"

if [ -z ${mode} ];then
    #check current mode
    ${php} bin/magento deploy:mode:show > bamboo_current_mode.txt

    if grep -q production "bamboo_current_mode.txt"; then
        mode=production
    else
        mode=developer
    fi

    rm -f bamboo_current_mode.txt
fi

#make sure the mode is what we expected
${php} bin/magento deploy:mode:set -s ${mode}


if [ ${mode} = "production" ];then
    ${php} bin/magento maintenance:enable

    clean_magento_files

    ${php} bin/magento setup:upgrade

    clean_magento_files

    ${php} bin/magento setup:di:compile

    ${php} bin/magento setup:static-content:deploy en_US ${static_content_params}

    ${php} bin/magento maintenance:disable

else
    clean_magento_files
    ${php} bin/magento setup:upgrade
fi

reindex

#make sure cache is enabled
${php} bin/magento cache:enable
${php} bin/magento cache:clean

external_cache_reset "${is_opcache}" "${is_redis}" "${redis_server_host}"