#!/usr/bin/env bash

is_force_use_folder=$1
project_clone_from_stash=$2
project_pwd=$3
php=$4
composer=$5
script_location=$6

#make sure folder exists
mkdir -p ${project_pwd}

if [ -z "${is_force_use_folder}" ];then
    is_force_use_folder=1
fi

if [ "$(ls -A ${project_pwd})" ]; then
    if [ ${is_force_use_folder} -eq 1 ];then
        echo "${project_pwd} detected, but it will be deleted due to force_use_folder option"
        #temp fix for "folder is already in used, multiple delete"
        rm -rf ${project_pwd}
        rm -rf ${project_pwd}
        rm -rf ${project_pwd}
        rm -rf ${project_pwd}
    else
        echo "${project_pwd} existed. Is this folder being used?"
        exit \1
    fi
fi

#create empty folder again if just removed recently
mkdir -p ${project_pwd}
cd ${project_pwd}

GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" git clone ${project_clone_from_stash} .

retvar=$?

if [ ${retvar} != 0 ];then
    echo "clone failed. It should be wrong url or wrong authentication"
    exit 1
fi

#to make sure it will run on composer, we install composer.phar anyway
#download composer.phar
${php} -r "copy('http://getcomposer.org/installer', 'composer-setup.php');"
${php} composer-setup.php
${php} -r "unlink('composer-setup.php');"

#install n98-magerun2.phar
wget https://files.magerun.net/n98-magerun2.phar

auth_json_path="${script_location}/Project/Setup/auth.json"
if [ -f "/home/$USER/.config/composer/auth.json" ];then
    auth_json_path="/home/$USER/.config/composer/auth.json"
fi

#composer install
#check if auth.json was created, if not, cp default auth.json
if [ ! -f "var/composer_home/auth.json" ];then
    echo "var/composer_home contain no auth.json ... \n";
    mkdir -p var/composer_home

    cp ${auth_json_path} var/composer_home/auth.json
fi

cp ${auth_json_path} auth.json

#echo "Make sure hirak/prestissimo is installed ..."
${composer} global require hirak/prestissimo
