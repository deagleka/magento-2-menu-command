#!/usr/bin/env bash

is_enable=$1
project_pwd=$2
php=$3

cd ${project_pwd}

if [ ${is_enable} -eq 1 ];then
    echo "Enabling Cron"
    ${php} bin/magento cron:install
else
    echo "Disabling Cron"
    ${php} bin/magento cron:remove
fi
