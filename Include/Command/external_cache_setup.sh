#!/usr/bin/env bash

project_path=$1
php=$2
is_used_redis=$3
is_used_varnish=$4
redis_server_host=$5
varnish_server_host=$6
varnish_server_port=$7
varnish_backend_port=$8

cd ${project_path}

if [ -z "${is_used_redis}" ];then
    echo "We will not install Redis"
else
    if [ ${is_used_redis} -eq 1 ];then
        #avoid issue Unable to unserialize value, string is corrupted.
        redis-cli -h ${redis_server_host} flushall

        ${php} bin/magento setup:config:set --page-cache=redis --page-cache-redis-server="${redis_server_host}" --page-cache-redis-db=1 --no-interaction
        ${php} bin/magento setup:config:set --cache-backend=redis --cache-backend-redis-server="${redis_server_host}" --cache-backend-redis-db=0 --no-interaction
        ${php} bin/magento setup:config:set --session-save=redis --session-save-redis-host="${redis_server_host}" --session-save-redis-log-level=3 --session-save-redis-db=2 --no-interaction
    fi
fi

if [ -z "${is_used_varnish}" ];then
    echo "We will not install Varnish"
else
    if [ ${is_used_varnish} -eq 1 ];then
        ${php} bin/magento config:set system/full_page_cache/caching_application 2
        ${php} bin/magento config:set system/full_page_cache/varnish/backend_host "${varnish_server_host}"
        ${php} bin/magento config:set system/full_page_cache/varnish/backend_port "${varnish_backend_port}"
        ${php} bin/magento setup:config:set --http-cache-hosts="${varnish_server_host}:${varnish_server_port}" --no-interaction
    fi
fi