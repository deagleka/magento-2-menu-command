#!/usr/bin/env bash

MUSER=$1
MPASS=$2
MDB=$3
MHOST=$4

function clean_database(){
    mysql -h${MHOST} -u${MUSER} -p${MPASS} -e "DROP DATABASE if exists ${MDB};"
    mysql -h${MHOST} -u${MUSER} -p${MPASS} -e "CREATE DATABASE if not exists ${MDB};"
}

function default_config(){
    frontend_base_url=$1

    #set default config in core_config_data
    echo "Configuring elastic search server ..."
    mysql -h${MHOST} -u${MUSER} -p${MPASS} ${MDB} \
        -e "INSERT INTO core_config_data(scope, scope_id, path, value) value('default','0','web/unsecure/base_url','${frontend_base_url}') ON DUPLICATE KEY UPDATE value='${frontend_base_url}';" \
        -e "INSERT INTO core_config_data(scope, scope_id, path, value) value('default','0','web/secure/base_url','${frontend_base_url}') ON DUPLICATE KEY UPDATE value='${frontend_base_url}';"
}

function set_admin_interface_locale(){
    admin_user=$1
    interface_locale="en_US"

    [ "$2" != "" ] && interface_locale="$2"

    mysql -h${MHOST} -u${MUSER} -p${MPASS} ${MDB} \
        -e "update admin_user set interface_locale='${interface_locale}' where username='${admin_user}'"

}

function update_database_config(){
    config_path=$1
    config_value=$2
    config_scope="default"
    config_scope_id=0

    [ "$3" != "" ] && config_scope="$3"
    [ "$4" != "" ] && config_scope_id="$4"

    mysql -h${MHOST} -u${MUSER} -p${MPASS} ${MDB} \
        -e "INSERT INTO core_config_data(scope, scope_id, path, value) value('${config_scope}','${config_scope_id}','${config_path}','${config_value}') ON DUPLICATE KEY UPDATE value='${config_value}';"
}