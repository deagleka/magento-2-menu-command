#!/usr/bin/env bash

real_file_path=$(realpath "${BASH_SOURCE[0]}")
script_location=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )

php=$1
composer=$2
project_pwd=$3
domain="http://127.0.0.1/"

[ "$4" != "" ] && domain="$4"

if [ -z "${php}" ];then
    php=php
fi

if [ -z "${composer}" ];then
    composer="php composer.phar"
fi

function connect_database(){
    MUSER=$1
    MPASS=$2
    MDB=$3

    MHOST=localhost
    [ "$4" != "" ] && MHOST="$4"

    source ${script_location}/Include/Config/database.sh "${MUSER}" "${MPASS}" "${MDB}" "${MHOST}"
}

function before_setup(){
    is_force_use_folder=$1
    project_clone_from_stash=$2

    bash ${script_location}/Include/Command/before_setup.sh \
    "${is_force_use_folder}" \
    "${project_clone_from_stash}" \
    "${project_pwd}" \
    "${php}" \
    "${composer}" \
    "${script_location}"
}

function clean_magento_files(){
    cd ${project_pwd}
    rm -rf generated/ var/cache/ var/page_cache/ var/view_preprocessed/
}

function grant_files_permission(){
    cd ${project_pwd}

    echo "grant file permission"
    #grant permission on files and folders
    find . -type d -exec chmod 755 {} \; \
    && find . -type f -exec chmod 644 {} \; \
    && chmod u+x bin/magento

    git config core.fileMode false
}

function add_ssh_key(){
    ssh_key_path=$1

    eval "$(ssh-agent -s)"
    ssh-add ${ssh_key_path}
    retvar=$?

    if [ ${retvar} != 0 ];then
        echo "ssh-add key failed. Please re-check"
        exit \1
    fi
}

function reindex(){
    cd ${project_pwd}
    echo "preparing to reindex..."

    #check if we need to run reindex
    ${php} bin/magento indexer:status > bamboo_indexer_status.txt

    if grep -q "Reindex required" "bamboo_indexer_status.txt"; then
        ${php} bin/magento indexer:reindex
    else
        ${php} bin/magento indexer:status
        echo "indexer is already done. No need to reindex "
    fi
    rm -f bamboo_indexer_status.txt
}

function magento_deploy_process(){
    is_opcache=$1
    is_redis=$2
    redis_server_host=$3
    static_content_params=""
    mode="production"

    [ "$4" != "" ] && static_content_params="$4"
    [ "$5" != "" ] && mode="$5"

    bash ${script_location}/Include/Command/magento_deploy_process.sh \
    "${project_pwd}" \
    "${php}" \
    "${static_content_params}" \
    "${mode}" \
    "${is_redis}" \
    "${is_opcache}" \
    "${redis_server_host}" \
    "${domain}"
}

function enable_cron(){
    is_enable=$1

    if [ -z ${is_enable} ];then
        is_enable=1
    fi

    bash ${script_location}/Include/Command/enable_cron.sh \
    "${is_enable}" \
    "${project_pwd}" \
    "${php}"
}

function external_cache_reset(){
    is_opcache=0
    is_redis=0
    redis_server_host="localhost"

    [ "$1" != "" ] && is_opcache="$1"
    [ "$2" != "" ] && is_redis="$2"
    [ "$3" != "" ] && redis_server_host="$3"

    bash ${script_location}/Include/Command/external_cache_reset.sh \
    "${project_pwd}" \
    "${is_opcache}" \
    "${is_redis}" \
    "${redis_server_host}" \
    "${domain}"
}

function external_cache_setup(){

    is_redis=0
    is_varnish=0
    redis_server_host="localhost"
    varnish_server_host="localhost"
    varnish_server_port=6082
    varnish_backend_port=6081

    [ "$1" != "" ] && is_redis="$1"
    [ "$2" != "" ] && is_varnish="$2"
    [ "$3" != "" ] && redis_server_host="$3"
    [ "$4" != "" ] && varnish_backend_host="$4"
    [ "$5" != "" ] && varnish_server_port="$5"
    [ "$6" != "" ] && varnish_backend_port="$6"

    bash ${script_location}/Include/Command/external_cache_setup.sh \
    "${project_pwd}" \
    "${php}" \
    "${is_redis}" \
    "${is_varnish}" \
    "${redis_server_host}" \
    "${varnish_server_host}" \
    "${varnish_server_port}" \
    "${varnish_backend_port}"

}

function add_admin_account(){
    admin_user=$1
    admin_password=$2
    admin_email="${admin_user}@email.com"
    admin_firstname="${admin_user}"
    admin_lastname="${admin_user}"

    [ "$3" != "" ] && admin_email="$3"
    [ "$4" != "" ] && admin_firstname="$4"
    [ "$5" != "" ] && admin_lastname="$5"

    cd ${project_pwd}

    ${php} bin/magento admin:user:create --admin-user=${admin_user} --admin-password=${admin_password} --admin-email=${admin_email} --admin-firstname="${admin_firstname}" --admin-lastname="${admin_lastname}"
}
