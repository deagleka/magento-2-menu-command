#!/usr/bin/env bash

CURRENT_MENU_DIR=$1

if [ -z "${CURRENT_MENU_DIR}" ];then
    real_file_path=$(realpath "${BASH_SOURCE[0]}")
    CURRENT_MENU_DIR=$( cd "$( dirname "${real_file_path}" )" >/dev/null 2>&1 && pwd )
fi

if [ ! -f ${CURRENT_MENU_DIR}/.env ];then
    touch ${CURRENT_MENU_DIR}/.env

    cat > ${CURRENT_MENU_DIR}/.env <<EOL
php=php
composer="php composer.phar"
project_pwd=""
ssh_key_path="/home/$USER/.ssh/id_rsa"
#when is_force_use_folder=0, if folder is not empty, will exit
is_force_use_folder=1
#git clone from stash (bitbucket|github)
project_clone_from_stash=""
db_username=""
db_password=""
db_name=""
db_host="localhost"
backend_path="admin"
deploy_branch="master"
frontend_base_url=""
deploy_mode="production"
git_remote="origin"
is_use_opcache=0
is_use_redis=0
is_use_varnish=0
#ignore theses params if dont use external caches
redis_server_host="localhost"
varnish_server_host="localhost"
varnish_server_port=6081
varnish_backend_port=8080
#additional locale from en_US with setup:static:content
static_content_params=""
EOL

    env_file_path=$(readlink -f ${CURRENT_MENU_DIR}/.env)
    echo "env file is missing, created default value in ${env_file_path}. \nPlease fill all params and try again..."
    exit \1
fi

#show all params
sed '' ${CURRENT_MENU_DIR}/.env