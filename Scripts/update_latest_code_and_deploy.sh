#!/usr/bin/env bash

real_file_path=$(realpath "${BASH_SOURCE[0]}")
DIR=$( cd "$( dirname "${real_file_path}" )" >/dev/null 2>&1 && pwd )

env_file_location="${DIR}/../.env"
[ "$3" != "" ] && env_file_location="$3"
source "${env_file_location}"
source ${DIR}/../index.sh "${php}" "${composer}" "${project_pwd}" "${frontend_base_url}"

[ "$1" != "" ] && deploy_branch="$1"

is_need_latest_code=1
[ "$2" != "" ] && is_need_latest_code="$2"

cd ${project_pwd}

if [ "${is_need_latest_code}" -eq 1 ];then
    add_ssh_key "${ssh_key_path}"
    git fetch --all --prune
    git checkout ${deploy_branch}

    git reset --hard ${git_remote}/${deploy_branch}
fi

modman repair --force
${composer} install --no-interaction --no-dev

magento_deploy_process \
"${is_use_opcache}" \
"${is_use_redis}" \
"${redis_server_host}" \
"${static_content_params}" \
"${deploy_mode}"
