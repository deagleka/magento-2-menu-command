#!/usr/bin/env bash

real_file_path=$(realpath "${BASH_SOURCE[0]}")
DIR=$( cd "$( dirname "${real_file_path}" )" >/dev/null 2>&1 && pwd )

env_file_location="${DIR}/../.env"
[ "$1" != "" ] && env_file_location="$1"
source "${env_file_location}"
source ${DIR}/../index.sh "${php}" "${composer}" "${project_pwd}" "${frontend_base_url}"

reindex