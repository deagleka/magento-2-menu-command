#!/usr/bin/env bash

real_file_path=$(realpath "${BASH_SOURCE[0]}")
DIR=$( cd "$( dirname "${real_file_path}" )" >/dev/null 2>&1 && pwd )

env_file_location="${DIR}/../.env"
[ "$6" != "" ] && env_file_location="$6"
source "${env_file_location}"
source ${DIR}/../index.sh "${php}" "${composer}" "${project_pwd}" "${frontend_base_url}"

admin_user=$1
admin_password=$2
admin_email=$3
admin_firstname=$4
admin_lastname=$5

add_admin_account \
    "${admin_user}" \
    "${admin_password}" \
    "${admin_email}" \
    "${admin_firstname}" \
    "${admin_lastname}"
