#!/usr/bin/env bash

real_file_path=$(realpath "${BASH_SOURCE[0]}")
DIR=$( cd "$( dirname "${real_file_path}" )" >/dev/null 2>&1 && pwd )

env_file_location="${DIR}/../.env"
[ "$1" != "" ] && env_file_location="$1"
source "${env_file_location}"
source ${DIR}/../index.sh "${php}" "${composer}" "${project_pwd}" "${frontend_base_url}"

#process starting from here
add_ssh_key "${ssh_key_path}"
before_setup "${is_force_use_folder}" "${project_clone_from_stash}"

connect_database "${db_username}" "${db_password}" "${db_name}" "${db_host}"
clean_database

cd ${project_pwd}
git checkout ${deploy_branch}

modman repair --force

${composer} install --no-interaction --no-dev

${php} bin/magento module:enable --all

#disable all custom module
echo "disable all custom module"
${php} bin/magento module:status | grep -v Magento | grep -v List | grep -v None | grep -v -e '^$'| xargs ${php} bin/magento module:disable -f

${php} bin/magento module:enable MSP_ReCaptcha Dotdigitalgroup_Email MSP_TwoFactorAuth

#setup database
${php} bin/magento setup:install \
--backend-frontname="${backend_path}" \
--db-host="${db_host}" --db-name="${db_name}" --db-user="${db_username}" --db-password="${db_password}" \
--admin-firstname=admin --admin-lastname=admin --admin-email=admin@admin.com --admin-user=admin --admin-password=admin123 \
--use-rewrites=1

default_config "${frontend_base_url}"

bash ${DIR}/setup_magento_external_cache.sh "${env_file_location}"

grant_files_permission
