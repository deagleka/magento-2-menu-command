###### How to Install.

`mkdir -p /home/$USER/menu-management`

`git clone git@bitbucket.org:deagleka/bash-scripts-oop.git /home/$USER/menu-management`

###### Support symlink for multiple project.

`mkdir -p ${manage_location_fullpath}`

`ln -s /home/$USER/menu-management/menu ${manage_location_fullpath}`


###### Running this script to start.

`cd ${manage_location_fullpath}`

`./menu`
